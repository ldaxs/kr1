public class VisCalc {
    private  final String visitID;
    private  final String adressVisitor;
    private  final String visitorID;
    private  final String visitDate;
    private  final String amount;
    private  final String locationVisit;
    private  final String ageVisitors;
    private  final String limitAmount;

    public String getVisitID() {
        return visitID;
    }

    public String getAdressVisitor() {
        return adressVisitor;
    }

    public String getVisitorID() {
        return visitorID;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public String getAmount() {
        return amount;
    }

    public String getLocationVisit() {
        return locationVisit;
    }

    public String getAgeVisitors() {
        return ageVisitors;
    }

    public String getLimitAmount() {
        return limitAmount;
    }

    @Override
    public String toString() {
        return "VisCalc{" +
                "visitID='" + visitID + '\'' +
                ", adressVisitor='" + adressVisitor + '\'' +
                ", visitorID='" + visitorID + '\'' +
                ", visitDate='" + visitDate + '\'' +
                ", amount='" + amount + '\'' +
                ", locationVisit='" + locationVisit + '\'' +
                ", ageVisitors='" + ageVisitors + '\'' +
                ", limitAmount='" + limitAmount + '\'' +
                '}';
    }

    private VisCalc(VisCalcBuilder builder){
        this.visitID = builder.visitID;
        this.visitorID = builder.visitorID;
        this.visitDate = builder.visitDate;
        this.amount = builder.amount;
        this.ageVisitors = builder.ageVisitors;
        this.adressVisitor = builder.adressVisitor;
        this.locationVisit = builder.locationVisit;
        this.limitAmount = builder.limitAmount;

    }

    public static class VisCalcBuilder {
        private  String visitID;
        private  String adressVisitor;
        private  String visitorID;
        private  String visitDate;
        private  String amount;
        private  String locationVisit;
        private  String ageVisitors;
        private  String limitAmount;

        public VisCalcBuilder(String visitID, String visitorID, String visitDate, String amount){
            this.visitID = visitID;
            this.visitorID = visitorID;
            this.visitDate = visitDate;
            this.amount = amount;
        }

        public VisCalcBuilder ageVisitors(String ageVisitors){
            this.ageVisitors = ageVisitors;
            return this;
        }

        public VisCalcBuilder adressVisitor(String adressVisitor){
            this.adressVisitor = adressVisitor;
            return this;
        }

        public VisCalcBuilder limitAmount(String limit){
            this.limitAmount = limitAmount;
            return this;
        }

        public VisCalcBuilder location(String locationVisit){
            this.locationVisit = locationVisit;
            return this;
        }

        public VisCalc build() {
            VisCalc calc = new VisCalc(this);
            return calc;
        }

    }
}
