import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        VisCalc[] arrCl = new VisCalc[100];
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        int count = 0;

        do {
            System.out.println("Press 1 to add information;\nPress 2 to display a list of information;\nPress 0 to complete entry;\n\n");
            System.out.println("Enter: ");
            num = scanner.nextInt();

            switch (num){
                case 1:
                    System.out.println("---------------------------------------");
                    String visitID;
                    String adressVisitor;
                    String visitorID;
                    String visitDate;
                    String amount;
                    String locationVisit;
                    String ageVisitors;
                    String limitAmount;

                    for(int i = count; i<count+1; i++){
                        scanner.nextLine();
                        System.out.println("Enter ID visit: ");
                        visitID = scanner.nextLine();
                        System.out.println("Enter visitor address: ");
                        adressVisitor = scanner.nextLine();
                        System.out.println("Enter ID visitor: ");
                        visitorID = scanner.nextLine();
                        System.out.println("Enter the amount spent: ");
                        amount = scanner.nextLine();
                        System.out.println("Enter visitor visit date: ");
                        visitDate = scanner.nextLine();
                        System.out.println("Enter age visitor: ");
                        ageVisitors = scanner.nextLine();
                        System.out.println("Enter the possible limit: ");
                        limitAmount = scanner.nextLine();

                        arrCl[i] = new VisCalc.VisCalcBuilder(visitID, visitorID, visitDate, amount).adressVisitor(adressVisitor)
                                .ageVisitors(ageVisitors).limitAmount(limitAmount).build();
                    }
                    System.out.println("\nInfo added!\n");
                    count++;
                    break;

                case 2:
                    System.out.println("\nList info:\n");
                    for(int j = 0; j < count; j++){
                        System.out.println(arrCl[j] + "\n");
                    }
                    break;

                default: break;
            }
        } while(num != 0);

    }
}